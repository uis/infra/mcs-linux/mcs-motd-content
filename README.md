# mcs-motd-content

This package contains one important file, `motd.txt`, which is used as the
message of the day by MCS Linux systems.  It is displayed on all MCS Linux
logins, and is intended to be used to inform users of upcoming downtime or
other things they might need to know.

Pushes to the `master` branch will automatically cause the MOTD to be updated
by means of GitLab Continuous Deployment.  If you don't want to use Git, you
can edit the file directly in GitLab.